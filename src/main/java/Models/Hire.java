package Models;

/**
 * Created by Karol on 27.06.2017.
 */
public class Hire {

    private int hireId;
    private int carId;
    private int driverId;

    public Hire() {
    }

    public Hire(int hireId, int carId, int driverId) {
        this.hireId = hireId;
        this.carId = carId;
        this.driverId = driverId;
    }

    public int getHireId() {
        return hireId;
    }

    public void setHireId(int hireId) {
        this.hireId = hireId;
    }

    public int getCarId() {
        return carId;
    }

    public void setCarId(int carId) {
        this.carId = carId;
    }

    public int getDriverId() {
        return driverId;
    }

    public void setDriverId(int driverId) {
        this.driverId = driverId;
    }

    @Override
    public String toString() {
        return "Hire : " +
                "hireId= " + hireId +
                " carId= " + carId +
                ", driverId= " + driverId;
    }
}
