package Models;

/**
 * Created by Karol on 27.06.2017.
 */
public class Drivers {

    private int driverId;
    private String name;
    private String surname;

    public Drivers() {
    }

    public Drivers(int driverId, String name, String surname) {
        this.driverId = driverId;
        this.name = name;
        this.surname = surname;
    }

    public int getDriverId() {
        return driverId;
    }

    public void setDriverId(int driverId) {
        this.driverId = driverId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Override
    public String toString() {
        return "Drivers : " +
                "driverId= " + driverId +
                ", name= " + name +
                ", surname= " + surname;
    }
}
