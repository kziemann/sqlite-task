package Models;

/**
 * Created by Karol on 27.06.2017.
 */
public class Cars {

    private int carId;
    private String carBrand;
    private int horsePower;

    public Cars() {
    }

    public Cars(int carId, String carBrand, int horsePower) {
        this.carId = carId;
        this.carBrand = carBrand;
        this.horsePower = horsePower;
    }

    public int getCarId() {
        return carId;
    }

    public void setCarId(int carId) {
        this.carId = carId;
    }

    public String getCarBrand() {
        return carBrand;
    }

    public void setCarBrand(String carBrand) {
        this.carBrand = carBrand;
    }

    public int getHorsePower() {
        return horsePower;
    }

    public void setHorsePower(int horsePower) {
        this.horsePower = horsePower;
    }

    @Override
    public String toString() {
        return "Cars : " +
                "carId= " + carId +
                ", carBrand= " + carBrand +
                ", horsePower= " + horsePower;
    }
}
