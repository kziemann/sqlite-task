import Models.Cars;
import Models.Drivers;
import Models.Hire;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Karol on 27.06.2017.
 */
public class CarRent {

    private Statement preparedStatement;
    private Connection connection;

    public static final String DRIVER = "org.sqlite.JDBC";
    public static final String DB_URL = "jdbc:sqlite:carsRent.db";

    public CarRent () {

        try {
            Class.forName(CarRent.DRIVER);
        } catch (ClassNotFoundException e) {
            System.err.println("Driver not found JDBC");
            e.printStackTrace();
        }

        try {
            connection = DriverManager.getConnection(DB_URL);
            preparedStatement = connection.createStatement();
        } catch (SQLException e) {
            System.err.println("Problem with connection");
            e.printStackTrace();
        }
        
        createTables();
    }

    public boolean createTables() {
        String createCars = "CREATE TABLE IF NOT EXISTS cars (carId INTEGER PRIMARY KEY AUTOINCREMENT, carBrand varchar(255), horsePower int)";
        String createDrivers = "CREATE TABLE IF NOT EXISTS drivers (driverId INTEGER PRIMARY KEY AUTOINCREMENT, name varchar(255), surname varchar(255))";
        String createHire = "CREATE TABLE IF NOT EXISTS hire (hireId INTEGER PRIMARY KEY AUTOINCREMENT, carId int, driverId int)";

        try {
            preparedStatement.execute(createCars);
            preparedStatement.execute(createDrivers);
            preparedStatement.execute(createHire);
        } catch (SQLException e) {
            System.err.println("Error when creating table");
            e.printStackTrace();
            return false;
        }

        return true;
    }

    public boolean insertCars (String carBrand, int horsePower) {

        try {
            PreparedStatement prep = connection.prepareStatement("insert into cars values (NULL, ?, ?)");
            prep.setString(1,carBrand);
            prep.setInt(2,horsePower);
            prep.execute();
        } catch (SQLException e) {
            System.err.println("Error when insering Cars");
            e.printStackTrace();
            return false;
        }

        return true;

    }
    public boolean insertDrivers (String name, String surname) {

        try {
            PreparedStatement prep = connection.prepareStatement("insert into drivers values (NULL, ?, ?)");
            prep.setString(1,name);
            prep.setString(2,surname);
            prep.execute();
        } catch (SQLException e) {
            System.err.println("Error when insering Drivers");
            e.printStackTrace();
            return false;
        }

        return true;

    }
    public boolean insertHire (int carId, int driverId) {

        try {
            PreparedStatement prep = connection.prepareStatement("insert into hire values (NULL, ?, ?)");
            prep.setInt(1,carId);
            prep.setInt(2,driverId);
            prep.execute();
        } catch (SQLException e) {
            System.err.println("Error when insering Hire");
            e.printStackTrace();
            return false;
        }

        return true;

    }

    public List<Cars> selectCars () {
        List<Cars> cars = new LinkedList<Cars>();

        try {
            ResultSet result = preparedStatement.executeQuery("SELECT * FROM cars");
            int id;
            String carBrand;
            int horsePower;
            while (result.next()) {
                id = result.getInt("carId");
                carBrand = result.getString("carBrand");
                horsePower = result.getInt("horsePower");
                cars.add(new Cars(id,carBrand,horsePower));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return cars;
    }

    public List<Drivers> selectDrivers () {
        List<Drivers> drivers = new LinkedList<Drivers>();

        try {
            ResultSet result = preparedStatement.executeQuery("SELECT * FROM drivers");
            int id;
            String name;
            String surname;
            while (result.next()) {
                id = result.getInt("driverId");
                name = result.getString("name");
                surname = result.getString("surname");
                drivers.add(new Drivers(id,name,surname));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return drivers;
    }


    public List<Hire> selectHire () {
        List<Hire> hire = new LinkedList<Hire>();

        try {
            ResultSet result = preparedStatement.executeQuery("SELECT * FROM hire");
            int id;
            int carId;
            int driverId;
            while (result.next()) {
                id = result.getInt("hireId");
                carId = result.getInt("carId");
                driverId = result.getInt("driverId");
                hire.add(new Hire(id,carId,driverId));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return hire;
    }

    public void closeConnection () {
        try {
            connection.close();
        } catch (SQLException e) {
            System.err.println("Error with quit connection");
            e.printStackTrace();
        }
    }


}
