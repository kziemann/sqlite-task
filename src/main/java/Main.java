import Models.Cars;
import Models.Drivers;
import Models.Hire;

import java.util.List;

///**
// * Created by Karol on 27.06.2017.
// */
public class Main {

    public static void main(String[] args) {

        CarRent carRent = new CarRent();

        carRent.createTables();
        List<Cars> cars = carRent.selectCars();
        List<Drivers> drivers = carRent.selectDrivers();
        List<Hire> hires = carRent.selectHire();
        for (Cars c : cars) {
            System.out.println(c);
        }
        for (Drivers d : drivers) {
            System.out.println(d);
        }
        for (Hire h : hires) {
            System.out.println(h);
        }

        carRent.closeConnection();



    }
}
